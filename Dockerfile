FROM python
WORKDIR /demo
ADD . /demo
RUN pip install Flask
CMD ["python" , "demo.py"]
